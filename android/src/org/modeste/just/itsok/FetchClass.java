package org.modeste.just.itsok;

import android.provider.ContactsContract;
import android.content.Context;
import android.database.Cursor;
import java.util.*;

public class FetchClass
{
    public FetchClass() {
    }

private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    public static String[] getContacts(Context c) {
        /**
          * Récupèration de la liste des contacts Mobile
          */
        ArrayList<String> contacts = new ArrayList<String>();
        Cursor phones = c.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        while (phones.moveToNext()) {
            int phoneType = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

            if (phoneType == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE) {
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                contacts.add(name + "|" + phoneNumber);
            }
        }

        contacts.sort(Comparator.naturalOrder());
        String fetch = "";


        for(String person : contacts) {
            if (fetch != "") {
                fetch += "|";
            }
            fetch += person;
            break;
        }
//        fetch += "</contacts>";

//        return fetch;
        return contacts.toArray(new String[0]);
    }
}
