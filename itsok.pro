QT += \
    core \
    quick \
    xml \
    sql \
    core-private

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    jmDatabase/Classes/jmdatabase.cpp \
    jmDatabase/Classes/jmfilequery.cpp \
    src/Classes/Entities/contact.cpp \
    src/Classes/Models/contactlistmodel.cpp \
    src/Classes/Repositories/contactdb.cpp \
    src/Classes/Repositories/contactphone.cpp \
    src/main.cpp

RESOURCES += \
    resources.qrc

TRANSLATIONS += \
    i18n/itsok_en_001.ts

CONFIG += \
    lrelease
    embed_translations

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ../../../../../share/MesProjets/Qt/itsok/android/AndroidManifest.xml \
    ../../../../../share/MesProjets/Qt/itsok/android/build.gradle \
    ../../../../../share/MesProjets/Qt/itsok/android/res/values/libs.xml \
    android/AndroidManifest.xml \
    android/src/org/modeste/just/itsok/FetchClass.java \
    android/src/org/modeste/just/itsok/FetchClass.java

contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

HEADERS += \
    jmDatabase/Classes/jmdatabase.h \
    jmDatabase/Classes/jmfilequery.h \
    src/Classes/Entities/contact.h \
    src/Classes/Models/contactlistmodel.h \
    src/Classes/Repositories/contactdb.h \
    src/Classes/Repositories/contactphone.h
