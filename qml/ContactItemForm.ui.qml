import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Item {
    id: iRoot
    width: 234
    height: 70

    property alias nameContact: nameContact.text
    property alias numberContact: numberContact.text
    property alias mouseArea: mouseArea
    property alias isSelectedContact: isSelectedContact.checked

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        Rectangle {
            radius: 5
            border.width: 1
            anchors.fill: parent
            anchors.margins: 5

            RowLayout {
                id: row
                anchors.fill: parent
                spacing: 10

                CheckBox {
                    id: isSelectedContact
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                }

                ColumnLayout {
                    id: column
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.margins: 10

                    Text {
                        id: nameContact
                        text: "name"
                        width: column.width
                        font.pixelSize: 16
                        Layout.fillWidth: true
                        font.bold: true
                    }

                    Text {
                        id: numberContact
                        text: "+336 12 23 34 45"
                        width: column.width
                        font.pixelSize: 13
                        horizontalAlignment: Text.AlignRight
                        Layout.fillWidth: true
                    }
                }
            }
        }
    }
}
