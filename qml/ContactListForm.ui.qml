import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import QtQml.XmlListModel

Item {
    id: iRoot
    width: 454
    height: 800

    signal selected;

    property alias title: title.text
    property alias list: listView
    property alias model: listView.model

    ColumnLayout {
        id: column
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        Label {
            id: title
            text: qsTr("Contacts in It's Ok")
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }

        Find {
            id: find
            visible: false
            Layout.fillWidth: true
        }

        Rectangle {
            color: "#ff0050"
            border.width: 1
            Layout.fillWidth: true
            Layout.fillHeight: true

            ListView {
                id: listView
                anchors.fill: parent
                clip: true

                delegate: ContactItem {
                    nameContact: name
                    numberContact: phone
                    isSelectedContact: isSelected
                    height: 70
                    width: listView.width
                    anchors.rightMargin: 0
                    anchors.leftMargin: 0
                }

            }
        }
    }
}



