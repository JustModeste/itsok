import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Item {
    width: 454
    height: 800

    property alias bContactsPhone: bContactsPhone
    property alias contactsList: contactsList
    property alias list: contactsList.list

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        ContactList {
            id: contactsList
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Button {
            id: bContactsPhone
            Layout.fillWidth: true
            text: qsTr("Find contact in phone")
        }
    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}
}
##^##*/
