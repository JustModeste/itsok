import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Item {
    property alias contactsList: contactsList
    property alias bAddSelected: bAddSelected
    property alias list: contactsList.list
    property alias model: contactsList.model

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        ContactList {
            id: contactsList
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Button {
            id: bAddSelected
            Layout.fillWidth: true
            text: qsTr("Add selected contact in it'sOk")
        }
    }

}
