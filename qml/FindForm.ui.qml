import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Rectangle {
    width: 454
    height: 60

    property alias eFind: eFind

    RowLayout {
        anchors.fill: parent
        anchors.margins: 10

        Text {
            text: qsTr("Find: ")
        }

        TextField {
            id: eFind
            text: ""
            Layout.fillWidth: true
        }
    }
}
