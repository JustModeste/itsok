import QtQuick
import QtQuick.Window
import QtQuick.Layouts

Window {
    id: window
    width: 454
    height: 800
    visible: true
    title: qsTr("it's OK")

    StackLayout {
        anchors.fill: parent
        id: stack
        currentIndex: 0

        ContactsDb {
            id: contactsDb
            contactsList.title: qsTr("Contacts in It's Ok")
            bContactsPhone.enabled: true

            bContactsPhone.onClicked: {
//                bContactsPhone.enabled = false
                stack.currentIndex++
                if (modelContactPhone.rowCount() === 0) {
                    contactPhone.refresh()
                }
            }
        }

        ContactsPhone {
            id: cPhone
            contactsList.title: qsTr("Contacts on phone")
            bAddSelected.enabled: true

            bAddSelected.onClicked: {
                contactDb.refresh()
//                bAddSelected.enabled = false
                stack.currentIndex--
            }

        }
    }
}
