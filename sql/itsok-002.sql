CREATE TABLE Contacts (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        VARCHAR(255),
    phone       VARCHAR(20)
);
