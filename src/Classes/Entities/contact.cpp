#include "contact.h"

Contact::Contact(QString name, QString phone, bool isSelected, QObject *parent)
    : QObject{parent}
{
    this->m_name = name;
    this->m_phone = phone;
    this->m_isSelected = isSelected;
}

const QString &Contact::name() const
{
    return m_name;
}

void Contact::setName(const QString &newName)
{
    if (m_name == newName)
        return;
    m_name = newName;
    emit nameChanged();
}

const QString &Contact::phone() const
{
    return m_phone;
}

void Contact::setPhone(const QString &newPhone)
{
    if (m_phone == newPhone)
        return;
    m_phone = newPhone;
    emit phoneChanged();
}

bool Contact::isSelected() const
{
    return m_isSelected;
}

void Contact::setIsSelected(bool newIsSelected)
{
    if (m_isSelected == newIsSelected)
        return;
    m_isSelected = newIsSelected;
    emit isSelectedChanged();
}
