#ifndef CONTACT_H
#define CONTACT_H

#include <QObject>
#include <QString>
#include <QDebug>

#include "jmDatabase/Classes/jmdatabase.h"

class Contact : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(bool isSelected READ isSelected WRITE setIsSelected NOTIFY isSelectedChanged)
public:
    explicit Contact(QString name = "", QString phone = "", bool isSelected = false, QObject *parent = nullptr);

    const QString &name() const;
    void setName(const QString &newName);

    const QString &phone() const;
    void setPhone(const QString &newPhone);

    bool isSelected() const;
    void setIsSelected(bool newIsSelected);

signals:

    void nameChanged();
    void phoneChanged();

    void isSelectedChanged();

private:
    QString m_name;
    QString m_phone;
    bool m_isSelected;
};

#endif // CONTACT_H
