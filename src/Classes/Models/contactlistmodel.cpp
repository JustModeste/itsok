#include "contactlistmodel.h"

ContactListModel::ContactListModel()
{
    this->_list.clear();
}

int ContactListModel::rowCount(const QModelIndex &parent) const
{
//    Q_UNUSED(parent)
    return this->_list.count();
}

QVariant ContactListModel::data(const QModelIndex &index, int role) const
{
    QVariant    ret;

    if (!index.isValid() || (index.row()< 0 || index.row() > this->_list.size())) {
        return ret;
    }

    Contact* item = this->_list.at(index.row());

    switch (role) {
    case NameRole:
        return item->name();
        break;
    case PhoneRole:
        return item->phone();
        break;
    case SelectedRole:
        return item->isSelected();
        break;
    default:
        return ret;
    }
}

bool ContactListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (this->_list.count() == 0)
        return false;

    Contact* item = this->_list[index.row()];
//            .items().at(index.row());
    switch (role) {
    case NameRole:
        item->setName(value.toString());
        break;
    case PhoneRole:
        item->setPhone(value.toString());
        break;
    case SelectedRole:
        item->setIsSelected(value.toBool());
    }

    this->_list[index.row()] = item;
    return true;
//    if (this->_list.setItemAt(index.row(), item)) {
//        emit dataChanged(index, index, QVector<int>() << role);
//        return true;
//    }
    return false;
}


QHash<int, QByteArray> ContactListModel::roleNames() const
{
    return {
        { NameRole, "name"},
        { PhoneRole, "phone"},
        { SelectedRole, "isSelected"}
    };
}

void ContactListModel::addContact(Contact *contact)
{
    beginResetModel();
    this->_list.append(contact);
    endResetModel();
}
/*
void ContactListModel::addNewContact(QString name, QString phone) {
    this->addContact(new Contact(name, phone));
}
*/

void ContactListModel::addContacts(QList<Contact *> list)
{
    beginResetModel();
    for (int i = 0; i < list.size(); i++) {
        qDebug() << list[i]->name();
        this->_list.append(list[i]);
    }
    endResetModel();

}

void ContactListModel::clear()
{
    this->_list.clear();
}

Contact* ContactListModel::getContact(int i) {
    return this->_list[i];
}
