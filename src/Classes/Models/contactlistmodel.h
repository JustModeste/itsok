#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include <QAbstractListModel>
#include <QDebug>
#include <QString>

#include "src/Classes/Entities/contact.h"

class ContactListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum MyRoles {
        NameRole = Qt::UserRole + 1,
        PhoneRole,
        SelectedRole
        };

    ContactListModel();

public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    void addContact(Contact* contact);

//    Q_INVOKABLE void addNewContact(QString name, QString phone);
    Q_INVOKABLE void addContacts(QList<Contact* > list);
    Q_INVOKABLE Contact* getContact(int i);
    void clear();

private:
    QList<Contact*> _list;

};

#endif // CONTACTLISTMODEL_H
