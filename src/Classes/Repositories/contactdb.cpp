#include "contactdb.h"

ContactDb::ContactDb(jmDatabase *jmdb, ContactListModel *model, QObject *parent)
    : QObject(parent), jmdb(jmdb), m_model(model)
{
}

/**
 * @brief ContactDb::getAllContacts         Retourne la liste des contacts en base de données
 *
 * @param jmdb          JmDatabase          Objet de la base de données
 *
 * @return              -
 */
QList<Contact *> ContactDb::getAllContacts(void)
{
    QList<Contact *> list;

    QString sql =
            "SELECT "
                "c.id, "
                "c.name, "
                "c.phone "
            "FROM Contacts c "
            "ORDER BY c.name, c.phone";

    QSqlQuery query(this->jmdb->getDb());

    if (query.prepare(sql)) {
        if (query.exec()) {
            while (query.next()) {
                Contact *contact = new Contact();

                contact->setName(query.value("name").toString());
                contact->setPhone(query.value("phone").toString());
                list.append(contact);
            }
        }
    } else {
        qDebug() << "Error";
    }
    return list;
}

/**
 * @brief ContactDb::findByNameAndPhone                 Recherche l'id du contact
 * @param name          QString                         Nom du contact à rechercher
 * @param phone         QString                         Numéro de téléphone à rechercher
 * @return              int                             identifiant du contact, -1 si non trouvé
 */
int ContactDb::findByNameAndPhone(QString name, QString phone) {
    int id = -1;
    QString sql =
            "SELECT "
                "c.id "
            "FROM Contacts c "
            "WHERE c.name = :name "
            "AND c.phone = :phone ";
    QSqlQuery query(this->jmdb->getDb());

    if (query.prepare(sql)) {
        query.bindValue(":name", name);
        query.bindValue(":phone", phone);
        if (query.exec()) {
            if (query.next()) {
                id = query.value("id").toInt();
            }
        }
    }
    return id;
}

/**
 * @brief Insert un contact en base de données
 *
 * @param name              QString                 Nom du contact
 * @param phone             QString                 Numéro de téléphone
 *
 * @return                  bool                    True si le contact a été ajouté
 */
bool ContactDb::insertDb(QString name, QString phone) {
    bool ret = false;
    qDebug() << "Insert..." << name << " - " << phone << "...";
    if ((name != "" && phone != "") && this->findByNameAndPhone(name, phone) == -1) {
        qDebug() << "YES";
        QString sql =
                "INSERT INTO Contacts"
                "   (name, phone) "
                "VALUES (:name, :phone)";
        QSqlQuery query(this->jmdb->getDb());

        if (query.prepare(sql)) {
            query.bindValue(":name", name);
            query.bindValue(":phone", phone);
            if (!query.exec()) {
                ret = false;
            }
        } else {
            qDebug() << "Can't prepare query: " << sql << ": " << query.lastError().text();
            ret = false;
        }
    }
    qDebug() << "result" << ret;
    return ret;
}

/**
 * @brief ContactDb::insertDb                       Insert un contact en Base de données
 * @param contact           Contact                 Le contact à enregister
 *
 * @return                  bool                    true si le contact a été ajouté
 */
//bool ContactDb::insertDb(Contact contact) {
//    return this->insertDb(contact.name(), contact.phone());
//}

/**
 * @brief ContactDb::refresh                    Raffraichie le model
 */
void ContactDb::refresh() {
    this->m_model->clear();
    this->m_model->addContacts(this->getAllContacts());
}
