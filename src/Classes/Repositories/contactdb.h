#ifndef CONTACTDB_H
#define CONTACTDB_H

//#include "src/Classes/Entities/contact.h"

#include "src/Classes/Models/contactlistmodel.h"
#include "src/Classes/Repositories/contactdb.h"
#include "jmDatabase/Classes/jmdatabase.h"

class ContactDb : public QObject
//        : public Contact
{
    Q_OBJECT
public:
    explicit ContactDb(jmDatabase *jmdb, ContactListModel *model, QObject *parent = nullptr);

    QList<Contact *> getAllContacts(void);

    Q_INVOKABLE bool insertDb(QString name, QString phone);
//    Q_INVOKABLE bool insertDb(Contact contact);
    Q_INVOKABLE void refresh(void);
private:
    jmDatabase *jmdb;
    ContactListModel* m_model;

    int findByNameAndPhone(QString name, QString phone);

};

#endif // CONTACTDB_H
