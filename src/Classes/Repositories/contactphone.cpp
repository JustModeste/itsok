#include "contactphone.h"

ContactPhone::ContactPhone(jmDatabase* jmDb, ContactListModel *model, QObject *parent)
    : QObject(parent), jmDb(jmDb), m_model(model)
{
}

Q_INVOKABLE QList<Contact *> ContactPhone::getAllContacts(void) {
    qDebug() << "getContact()";

    QList<Contact *> list = QList<Contact *>();

#ifdef Q_PROCESSOR_ARM
    qDebug() << "Android context :" << QNativeInterface::QAndroidApplication::context();

    auto r = QtAndroidPrivate::checkPermission(QtAndroidPrivate::Contacts).result();
    if (r == QtAndroidPrivate::Denied) {
        r = QtAndroidPrivate::requestPermission(QtAndroidPrivate::Contacts).result();
    }
    if (r == QtAndroidPrivate::Denied) {
        return list;
    }
    QJniEnvironment env;
    jclass javaClass = env.findClass("org/modeste/just/itsok/FetchClass");

    QJniObject contacts = QJniObject::callStaticObjectMethod(
                javaClass,
                "getContacts",
                "(Landroid/content/Context;)[Ljava/lang/String;",
                QNativeInterface::QAndroidApplication::context()
    );

    jobjectArray contactArray = contacts.object<jobjectArray>();
    const int n = env->GetArrayLength(contactArray);

    QString oldContact = "";
    for (int i = 0; i < n; ++i) {
        jstring str = (jstring)(env->GetObjectArrayElement(contactArray, i));

        QString contact = env->GetStringUTFChars(str, 0);

        if (contact != oldContact) {
            qDebug() << contact;
            QStringList person = contact.split("|");
            qDebug() << "name: " << person[0] << " - phone: " << person.at(1);

            this->m_model->addContact(new Contact(person[0], person[1]));
            oldContact = contact;
        }
    }
#else
    list.append(new Contact("Alpha", "01 02 03 04 05"));
    list.append(new Contact("Bravo", "02 03 04 05 06"));
    list.append(new Contact("Chalie", "03 04 05 06 07"));
    list.append(new Contact("Delta", "004 05 06 07 08"));
    list.append(new Contact("Echo", "05 06 07 08 09"));
    list.append(new Contact("Foxtrot", "05 06 07 08 09"));
    list.append(new Contact("Golf", "01 02 03 04 05"));
    list.append(new Contact("Hotel", "02 03 04 05 06"));
    list.append(new Contact("India", "03 04 05 06 07"));
    list.append(new Contact("Juliett", "004 05 06 07 08"));
    list.append(new Contact("Kilo", "05 06 07 08 09"));
    list.append(new Contact("Lima", "01 02 03 04 05"));
    list.append(new Contact("Mike", "02 03 04 05 06"));
    list.append(new Contact("November", "03 04 05 06 07"));
    list.append(new Contact("Oscar", "004 05 06 07 08"));
    list.append(new Contact("Papa", "05 06 07 08 09"));
    list.append(new Contact("Québec", "01 02 03 04 05"));
    list.append(new Contact("Roméo", "02 03 04 05 06"));
    list.append(new Contact("Sierra", "03 04 05 06 07"));
    list.append(new Contact("Tango", "004 05 06 07 08"));
    list.append(new Contact("Uniform", "05 06 07 08 09"));
    list.append(new Contact("Victor", "01 02 03 04 05"));
    list.append(new Contact("Wiskey", "02 03 04 05 06"));
    list.append(new Contact("X-ray", "03 04 05 06 07"));
    list.append(new Contact("Yankee", "004 05 06 07 08"));
    list.append(new Contact("Zulu", "05 06 07 08 09"));
#endif
    return list;
}

/*
ContactListModel* ContactPhone::getList()
{
    return this->m_model;
}
*/

void ContactPhone::refresh() {
    this->m_model->clear();
    this->m_model->addContacts(this->getAllContacts());
}
