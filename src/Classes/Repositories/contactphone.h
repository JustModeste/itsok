#ifndef CONTACTPHONE_H
#define CONTACTPHONE_H

#include <QObject>
#include <QString>
#include <QDebug>
#include <QCoreApplication>
#include <QJniObject>
#include <QJniEnvironment>

#ifdef Q_PROCESSOR_ARM
#include <QtCore/private/qandroidextras_p.h>
#endif

#include "src/Classes/Models/contactlistmodel.h"
#include "src/Classes/Repositories/contactdb.h"
#include "jmDatabase/Classes/jmdatabase.h"

class ContactPhone : public QObject
{
    Q_OBJECT
public:
    explicit ContactPhone(jmDatabase *jmDb, ContactListModel *model, QObject *parent = nullptr);

signals:

public slots:
    QList<Contact *> getAllContacts();
//    Q_INVOKABLE ContactListModel* getList();
    Q_INVOKABLE void refresh(void);
private:
    jmDatabase* jmDb;
    ContactListModel* m_model;
};

#endif // CONTACTPHONE_H
