#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QList>
#include <QLocale>
#include <QTranslator>
#include <QQmlContext>

#include "src/Classes/Models/contactlistmodel.h"
#include "src/Classes/Repositories/contactdb.h"
#include "src/Classes/Repositories/contactphone.h"

#include "jmDatabase/Classes/jmdatabase.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "itsok_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    jmDatabase *jmDb = new jmDatabase("", "itsok.db", "itsokUser", "itsokPass");

    jmDb->open();



    QQmlApplicationEngine engine;

    ContactListModel *modelContactDb = new ContactListModel();
    ContactListModel *modelContactPhone = new ContactListModel();

    ContactDb* contactDb = new ContactDb(jmDb, modelContactDb);
    ContactPhone* contactPhone = new ContactPhone(jmDb, modelContactPhone);
    contactDb->refresh();

    engine.rootContext()->setContextProperty("contactDb", contactDb);
    engine.rootContext()->setContextProperty("contactPhone", contactPhone);
    engine.rootContext()->setContextProperty("modelContactDb", modelContactDb);
    engine.rootContext()->setContextProperty("modelContactPhone", modelContactPhone);


    const QUrl url(QStringLiteral("qrc:qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
